package helper;

import ia.battle.core.ConfigurationManager;

public class Config {
    public static final int TERRAIN_TYPE_COST_MULTIPLIER = 10;

    public static ConfigurationManager cf() {
        return ConfigurationManager.getInstance();
    }

    public static int maxSpeedNeeded() {
        // max speed to perform a "L" path from a corner to another considering that some points can cost more
        return (int) Math.ceil(1.2 * (cf().getMapWidth() + cf().getMapHeight()));
    }
}
