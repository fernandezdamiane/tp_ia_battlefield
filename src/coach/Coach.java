package coach;

import ia.battle.core.WarriorManager;
import warrior.profile.WarriorProfile;

import java.util.ArrayList;

public interface Coach{
    ArrayList<WarriorProfile> getProfilesCarrousel();
    int getCount();
    String getName();
}
