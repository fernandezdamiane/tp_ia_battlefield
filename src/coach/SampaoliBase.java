package coach;

import ia.battle.core.*;
import ia.exceptions.RuleException;
import warrior.DynamicWarrior;
import warrior.profile.WarriorProfile;
import warrior.profile.WarriorProfileFactory;


public class SampaoliBase implements GeneralListener {

	private int kills = 0;
	private int deaths = 0;
	private WarriorProfile currentProfile;
	private DynamicWarrior currentWarrior;
	private Coach coach;


	public SampaoliBase(Coach coach) {
		this.coach = coach;
		BattleField.getInstance().addListener(this);
		WarriorProfileFactory.setProfilesCarousel(coach.getProfilesCarrousel());
	}

	private int getGlobalBalance() {
		return kills - deaths;
	}

	public Warrior getNextWarrior() throws RuleException {
		WarriorProfile wp = getNextProfile(false);
		currentWarrior = new DynamicWarrior(wp.getName(), wp);
		return currentWarrior;
	}

	private WarriorProfile getNextProfile(boolean forceNewProfile) {
		currentProfile = currentProfile != null && currentProfile.getRecentBalance() >= 0 && !forceNewProfile ?
			currentProfile : WarriorProfileFactory.getNext();
		return currentProfile;
	}

	@Override
	public void startFight() {

	}

	@Override
	public void figthFinished(WarriorManager warriorManager) {
		BattleField.getInstance().addListener(this);
	}

	@Override
	public boolean continueFighting() {
		return true;
	}

	@Override
	public void warriorAttacked(Warrior warrior, Warrior warrior1, int i) {

	}

	@Override
	public void warriorKilled(Warrior warrior) {

	}

	@Override
	public void warriorMoved(Warrior warrior, FieldCell fieldCell, FieldCell fieldCell1) {

	}

	@Override
	public void worldChanged(FieldCell fieldCell, FieldCell fieldCell1) {

	}

	@Override
	public void statsChanged(String s, int i, String s1, int i1) {
		int currentKills = (s == coach.getName() ? i1 : i) - 1;
		int currentDeaths = (s == coach.getName() ? i : i1) - 1;
		if (currentKills - kills > 0) {
			kills = currentKills;
			if (currentProfile != null)
				currentProfile.addKills();
		}
		if (currentDeaths - deaths > 0) {
			deaths = currentDeaths;
			if (currentProfile != null)
				currentProfile.addDeaths();
		}
		//System.out.println("stats- s " + s + " -  i: " + i+ " - s1" + s1+" - i1" +i1 + " -- " + kills + " - " + deaths + " - " + getGlobalBalance() );
	}

	@Override
	public void tickLapsed(long l) {

	}

	@Override
	public void turnLapsed(long l, int i, Warrior warrior) {
		if (currentWarrior != null && currentWarrior.getTurnsWithProfile() > 10
				&& getGlobalBalance() <= 0 && currentProfile.getRecentBalance() < 1) {
			System.out.println("Sampaoli has decided to change the Warrior Profile");
			currentWarrior.setWarriorProfile(getNextProfile(true));
		}
	}
}
