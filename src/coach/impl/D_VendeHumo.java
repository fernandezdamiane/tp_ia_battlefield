package coach.impl;

import coach.Coach;
import coach.SampaoliBase;
import ia.battle.core.Warrior;
import ia.battle.core.WarriorManager;
import ia.exceptions.RuleException;
import warrior.profile.WarriorProfile;
import warrior.profile.WarriorProfileFactory;

import java.util.ArrayList;

public class D_VendeHumo extends WarriorManager implements Coach {
    private SampaoliBase base;

    public D_VendeHumo() {
        base = new SampaoliBase(this);
    }

    @Override
    public String getName() {
        return "Vende Humo";
    }

    @Override
    public Warrior getNextWarrior() throws RuleException {
        return base.getNextWarrior();
    }

    @Override
    public ArrayList<WarriorProfile> getProfilesCarrousel() {
        ArrayList<WarriorProfile> profilesCarousel = new ArrayList<>();
        profilesCarousel.add(WarriorProfileFactory.createShadowAttacker());
        profilesCarousel.add(WarriorProfileFactory.createAttacker());
        profilesCarousel.add(WarriorProfileFactory.createSteadyShadowAttacker());
        profilesCarousel.add(WarriorProfileFactory.createARambo());
        return profilesCarousel;
    }
}
