package grid.pathfinding.astar.impl;

import helper.Config;
import ia.battle.core.FieldCell;
import ia.battle.core.FieldCellType;
import grid.pathfinding.astar.core.AbstractNode;

public class BattlefieldNode extends AbstractNode {

    private final FieldCell fieldCell;

    public BattlefieldNode(FieldCell fieldCell) {
        super(fieldCell.getX(), fieldCell.getY());
        this.fieldCell = fieldCell;
        this.setWalkable(fieldCell.getFieldCellType() != FieldCellType.BLOCKED);
        if (fieldCell.getCost() > 0 && fieldCell.getCost() < Float.POSITIVE_INFINITY) {
            this.increaseMovementPanelty((int)(fieldCell.getCost() * Config.TERRAIN_TYPE_COST_MULTIPLIER));
        }
    }

    public void sethCosts(AbstractNode endNode) {
        // manhattan method
        int abs = Math.abs(this.getxPosition() - endNode.getxPosition()) +
                  Math.abs(this.getyPosition() - endNode.getyPosition());
        this.sethCosts(abs * BASICMOVEMENTCOST);
    }

    public FieldCell getFieldCell() {
        return fieldCell;
    }
}
