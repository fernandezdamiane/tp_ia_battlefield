package grid.pathfinding.astar.impl;


import grid.utils.Grid;
import helper.Config;
import ia.battle.core.*;
import grid.pathfinding.astar.core.AbstractNode;
import grid.pathfinding.astar.core.Map;
import grid.pathfinding.astar.core.NodeFactory;
import warrior.DynamicWarrior;

import java.util.ArrayList;
import java.util.List;

public class BattlefieldPathFinder implements NodeFactory {

    private Map<BattlefieldNode> astarMap;
    private DynamicWarrior warrior;

    public BattlefieldPathFinder(DynamicWarrior warrior) {
        astarMap = new Map<BattlefieldNode>(
            Config.cf().getMapWidth(), Config.cf().getMapHeight(), this, false);
        this.warrior = warrior;
    }

    public List<BattlefieldNode> findPath(FieldCell pos) {
        return getMap().findPath(warrior.getPosition().getX(), warrior.getPosition().getY(), pos.getX(), pos.getY());
    }

    public List<BattlefieldNode> findPath(int x, int y) {
        return getMap().findPath(warrior.getPosition().getX(), warrior.getPosition().getY(), x, y);
    }

    public ArrayList<FieldCell> toFieldCellList(List<BattlefieldNode> path) {
        ArrayList<FieldCell> l = new ArrayList<FieldCell>();
        for (BattlefieldNode node : path) {
            l.add(node.getFieldCell());
        }
        return l;
    }

    @Override
    public AbstractNode createNode(int x, int y) {
        return new BattlefieldNode(Grid.bf().getFieldCell(x, y));
    }

    public Map<BattlefieldNode> getMap() {
        return astarMap;
    }

    public BattlefieldNode getNode(int x, int y) {
        return getMap().getNode(x, y);
    }

    public BattlefieldNode getNode(FieldCell cell) {
        return getNode(cell.getX(), cell.getY());
    }
}
