package grid.modifier;

public interface GridModifier {
    void modify();
}
