package grid.modifier.impl;

import grid.pathfinding.astar.impl.BattlefieldPathFinder;
import grid.utils.Grid;
import ia.battle.core.FieldCell;

public class EnemyGridModifier extends PositionRadiusGridModifier {
    private boolean escapeMode;

    public EnemyGridModifier(BattlefieldPathFinder pathFinder, boolean escapeMode) {
        super(pathFinder, escapeMode);
    }

    @Override
    protected int getRadius() {
        return 10;
    }

    @Override
    protected FieldCell getTargetPosition() {
        return Grid.bf().getEnemyData().getFieldCell();
    }
}
