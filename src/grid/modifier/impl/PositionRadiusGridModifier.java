package grid.modifier.impl;

import ia.battle.core.FieldCell;
import grid.pathfinding.astar.impl.BattlefieldPathFinder;
import grid.utils.Grid;

public abstract class PositionRadiusGridModifier {
    private final int DEFAULT_PENALTY_DELTA = 100;
    private final BattlefieldPathFinder pathFinder;
    private final boolean escapeMode;

    public PositionRadiusGridModifier(BattlefieldPathFinder pathFinder, boolean escapeMode) {
        this.escapeMode = escapeMode;
        this.pathFinder = pathFinder;
    }

    protected abstract int getRadius();

    protected abstract FieldCell getTargetPosition();

    protected int getPenaltyDelta() {
        return DEFAULT_PENALTY_DELTA * (escapeMode ? 1 : -1);
    };

    public void modify() {
        for (FieldCell fieldCell : Grid.cellsSorrounding(getTargetPosition(), getRadius())) {
            this.pathFinder.getNode(fieldCell).increaseMovementPanelty(getPenaltyDelta());
        }
    }
}
