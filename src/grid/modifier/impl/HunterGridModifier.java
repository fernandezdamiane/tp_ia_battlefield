package grid.modifier.impl;

import grid.pathfinding.astar.impl.BattlefieldPathFinder;
import grid.utils.Grid;
import ia.battle.core.FieldCell;

public class HunterGridModifier extends PositionRadiusGridModifier {
    public HunterGridModifier(BattlefieldPathFinder pathFinder) {
        super(pathFinder, true);
    }

    @Override
    protected int getRadius() {
        return 12;
    }

    @Override
    protected FieldCell getTargetPosition() {
        return Grid.bf().getHunterData().getFieldCell();
    }
}
