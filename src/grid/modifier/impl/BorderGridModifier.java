package grid.modifier.impl;

import grid.pathfinding.astar.impl.BattlefieldPathFinder;
import grid.utils.Grid;
import ia.battle.core.FieldCell;

public class BorderGridModifier {
    private final int DEFAULT_PENALTY_DELTA = 50;
    private final BattlefieldPathFinder pathFinder;

    public BorderGridModifier(BattlefieldPathFinder pathFinder) {
        this.pathFinder = pathFinder;
    }

    protected int getPenaltyDelta() {
        return DEFAULT_PENALTY_DELTA;
    };

    public void modify() {
        for (FieldCell fieldCell : Grid.lateralBorderCells(1)) {
            this.pathFinder.getNode(fieldCell).increaseMovementPanelty(getPenaltyDelta());
        }
    }
}
