package grid.utils;

import grid.pathfinding.astar.impl.BattlefieldPathFinder;
import grid.utils.comparator.DistanceComparator;
import grid.utils.comparator.GridPenaltyComparator;
import grid.utils.comparator.ListsIndexComparator;
import helper.Config;
import ia.battle.core.BattleField;
import ia.battle.core.FieldCell;
import ia.battle.core.FieldCellType;

import java.util.ArrayList;

public class Grid {

    public static BattleField bf() {
        return BattleField.getInstance();
    }

    public static int distanceTo(FieldCell a, FieldCell b) {
        return (int)(bf().calculateDistance(a, b) * 100);
    }

    public static ArrayList<FieldCell> cellsAsList(boolean onlyWalkable) {
        ArrayList<FieldCell> cells = new ArrayList<FieldCell>();
        for (FieldCell[] row : bf().getMap()) {
            for (FieldCell cell : row) {
                cells.add(cell);
            }
        }
        if (onlyWalkable) {
            cells = filterWalkable(cells);
        }
        return cells;
    }

    public static ArrayList<FieldCell> cellsByDistance(FieldCell target, boolean closestFist, boolean onlyWalkable) {
        ArrayList<FieldCell> cells = cellsAsList(onlyWalkable);
        cells.sort(new DistanceComparator(target, closestFist));
        return cells;
    }

    public static ArrayList<FieldCell> cellsByGridPenalty(BattlefieldPathFinder pathFinder, boolean onlyWalkable) {
        ArrayList<FieldCell> cells = cellsAsList(onlyWalkable);
        cells.sort(new GridPenaltyComparator(pathFinder));
        return cells;
    }

    public static ArrayList<FieldCell> filterWalkable(ArrayList<FieldCell> cells) {
        cells = (ArrayList<FieldCell>) cells.clone();
        cells.removeIf(fieldCell -> fieldCell.getFieldCellType() == FieldCellType.BLOCKED);
        return cells;
    }

    public static ArrayList<FieldCell> cellsSorrounding(FieldCell cell, int tiles) {
        int xFrom = Math.max(cell.getX() - tiles, 0);
        int xTo = Math.min(cell.getX() + tiles, Config.cf().getMapWidth() - 1);
        int yFrom = Math.max(cell.getY() - tiles, 0);
        int yTo = Math.min(cell.getY() + tiles, Config.cf().getMapHeight() - 1);
        return cellsFromRect(xFrom, xTo, yFrom, yTo);
    }

    public static ArrayList<FieldCell> cellsFromRect(int xFrom, int xTo, int yFrom, int yTo) {
        ArrayList<FieldCell> cells = new ArrayList<>();
        for (int x = xFrom; x <= xTo; x++) {
            for (int y = yFrom; y <= yTo; y++) {
                cells.add(Grid.bf().getFieldCell(x, y));
            }
        }
        return cells;
    }

    public static ArrayList<FieldCell> orderCellsByListsIndex(ArrayList<FieldCell>[] lists) {
        ArrayList<FieldCell> result = (ArrayList<FieldCell>) lists[0].clone();
        result.sort(new ListsIndexComparator(lists));
        return result;
    }

    public static ArrayList<FieldCell> lateralBorderCells(int tiles) {
        int xl = findBorderX(true);
        int xr = findBorderX(false);
        tiles--; // a tile is implicit
        ArrayList<FieldCell> cells = cellsFromRect(xl, xl+tiles, 0, Config.cf().getMapHeight()-1);
        cells.addAll(cellsFromRect(xr-tiles, xr, 0, Config.cf().getMapHeight()-1));
        return cells;
    }

    private static int findBorderX(boolean leftBorder) {
        int WIDTH = Config.cf().getMapWidth();
        int HEIGHT = Config.cf().getMapHeight();
        int x;
        for (x = leftBorder ? 0 : WIDTH-1; (leftBorder && x < WIDTH) || (!leftBorder && x > 0); x = leftBorder ? x+1 : x-1) {
            int y;
            for (y=0; y < HEIGHT; y++) {
                FieldCell cell = Grid.bf().getFieldCell(x, y);
                if (cell.getFieldCellType() != FieldCellType.BLOCKED) {
                    break; // A Cell in the column is not blocked, its not the edge.
                }
            }
            if (y != HEIGHT) {
                break; // The y Coord has not reached the last cell+1 then we have found the first edge column.
            }
        }
        return x;
    }
}
