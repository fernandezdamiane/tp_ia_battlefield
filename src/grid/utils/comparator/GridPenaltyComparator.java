package grid.utils.comparator;

import grid.pathfinding.astar.impl.BattlefieldPathFinder;
import grid.utils.Grid;
import ia.battle.core.FieldCell;

import java.util.Comparator;

public class GridPenaltyComparator implements Comparator<FieldCell> {

    private BattlefieldPathFinder pathFinder;

    public GridPenaltyComparator(BattlefieldPathFinder pathFinder) {
        this.pathFinder = pathFinder;
    }

    @Override
    public int compare(FieldCell a, FieldCell b) {
        return pathFinder.getNode(a).getMovementPanelty() - pathFinder.getNode(b).getMovementPanelty();
    }
};