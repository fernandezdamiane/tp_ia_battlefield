package grid.utils.comparator;

import grid.utils.Grid;
import ia.battle.core.FieldCell;

import java.util.ArrayList;
import java.util.Comparator;

public class ListsIndexComparator implements Comparator<FieldCell> {
    private ArrayList<FieldCell>[] lists;

    public ListsIndexComparator(ArrayList<FieldCell>[] lists) {
        this.lists = lists;
    }

    @Override
    public int compare(FieldCell a, FieldCell b) {
        return computeIndexWeight(a) - computeIndexWeight(b);
    }

    private int computeIndexWeight(FieldCell field) {
        int weight = 0;
        for (ArrayList<FieldCell> l : lists) {
            weight += l.indexOf(field);
        }
        return weight;
    }
};