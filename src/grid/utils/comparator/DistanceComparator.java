package grid.utils.comparator;

import grid.utils.Grid;
import ia.battle.core.FieldCell;

import java.util.Comparator;

public class DistanceComparator implements Comparator<FieldCell> {
    private final FieldCell target;
    private boolean closestFirst;

    public DistanceComparator(FieldCell target, boolean closestFirst) {
        this.target = target;
        this.closestFirst = closestFirst;
    }

    @Override
    public int compare(FieldCell a, FieldCell b) {
        int cmp = (Grid.distanceTo(a, target) - Grid.distanceTo(b, target));
        cmp = cmp * (closestFirst ? 1 : -1);
        // System.out.println("["+cmp+"]<"+target.getX()+","+target.getY()+"> - ("+a.getX()+","+a.getY()+") ("+b.getX()+","+b.getY()+")") ;
        return cmp;
    }
};