package warrior.strategy.impl;

import grid.utils.Grid;
import ia.battle.core.WarriorData;
import ia.battle.core.actions.Action;
import ia.battle.core.actions.Attack;
import ia.battle.core.actions.Skip;
import warrior.DynamicWarrior;
import warrior.move.EscapeOrFollowMovement;
import warrior.strategy.PlayerEstrategy;

public class EscapeOrAttackPlayerStrategy implements PlayerEstrategy {
    private final boolean escapeMode;

    public EscapeOrAttackPlayerStrategy(boolean escapeMode) {
        this.escapeMode = escapeMode;
    }

    @Override
    public Action playTurn(DynamicWarrior warrior, long tick, int actionNumber) {
        WarriorData enemy = Grid.bf().getEnemyData();
        if (enemy.getInRange()) {
            return new Attack(enemy.getFieldCell());
        }
        EscapeOrFollowMovement movement = new EscapeOrFollowMovement(warrior, this.escapeMode);
        if (movement.test()) {
            return movement;
        }
        return new Skip();
    }
}
