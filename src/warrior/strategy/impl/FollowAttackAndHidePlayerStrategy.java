package warrior.strategy.impl;

import grid.utils.Grid;
import helper.Config;
import ia.battle.core.WarriorData;
import ia.battle.core.actions.Action;
import ia.battle.core.actions.Attack;
import ia.battle.core.actions.Skip;
import warrior.DynamicWarrior;
import warrior.move.EscapeOrFollowMovement;
import warrior.strategy.PlayerEstrategy;

public class FollowAttackAndHidePlayerStrategy implements PlayerEstrategy {
    @Override
    public Action playTurn(DynamicWarrior warrior, long tick, int actionNumber) {
        EscapeOrFollowMovement movement;
        WarriorData enemy = Grid.bf().getEnemyData();
        if (actionNumber == 0) {
            movement = new EscapeOrFollowMovement(warrior, false);
            if (movement.test()) {
                return movement;
            }

        }
        if (actionNumber ==  Config.cf().getActionsPerTurn()-1) {
            movement = new EscapeOrFollowMovement(warrior, true);
            if (movement.test()) {
                return movement;
            }
        }
        if (enemy.getInRange()) {
            return new Attack(enemy.getFieldCell());
        }
        // At hit step, but can not reach the enemy yet, so lets follow he.
        movement = new EscapeOrFollowMovement(warrior, false);
        if (movement.test()) {
            return movement;
        }
        movement = new EscapeOrFollowMovement(warrior, true);
        if (movement.test()) {
            return movement;
        }
        return new Skip();
    }
}
