package warrior.strategy;

import ia.battle.core.actions.Action;
import warrior.DynamicWarrior;

public interface PlayerEstrategy {
    Action playTurn(DynamicWarrior warrior, long tick, int actionNumber);
}
