package warrior.move;

import grid.modifier.impl.BorderGridModifier;
import grid.modifier.impl.EnemyGridModifier;
import grid.modifier.impl.HunterGridModifier;
import ia.battle.core.ConfigurationManager;
import ia.battle.core.FieldCell;
import ia.battle.core.actions.Move;
import grid.pathfinding.astar.impl.BattlefieldNode;
import grid.pathfinding.astar.impl.BattlefieldPathFinder;
import warrior.DynamicWarrior;
import grid.utils.Grid;

import java.util.ArrayList;
import java.util.List;

public class EscapeOrFollowMovement extends Move {

	private final DynamicWarrior warrior;
	private final BattlefieldPathFinder pathFinder;
	private final ConfigurationManager config;
	private final boolean escapeMode;
	private FieldCell dest = null;
	private List<BattlefieldNode> path = null;

	public EscapeOrFollowMovement(DynamicWarrior warrior, boolean escapeMode) {
		this.config = ConfigurationManager.getInstance();
		this.warrior = warrior;
		this.pathFinder = new BattlefieldPathFinder(warrior);
		this.escapeMode = escapeMode;
		new HunterGridModifier(pathFinder).modify();
		new EnemyGridModifier(pathFinder, escapeMode).modify();
        new BorderGridModifier(pathFinder).modify();
	}

	public boolean test () {
		return getDestiny() != null && getDestiny() != warrior.getPosition() && getPath().size() > 0;
	}

	@Override
	public ArrayList<FieldCell> move() {
		return pathFinder.toFieldCellList(getPath());
	}

	private FieldCell getDestiny() {
		if (dest == null) {
			FieldCell enemyPos = Grid.bf().getEnemyData().getFieldCell();
			if (escapeMode) {
				FieldCell hunterPos = Grid.bf().getHunterData().getFieldCell();
				ArrayList<FieldCell> hunterFarthestCells = Grid.cellsByDistance(hunterPos, false, true);
				ArrayList<FieldCell> enemyFarthestCells = Grid.cellsByDistance(enemyPos, false, true);
				ArrayList<FieldCell> gridPenaltyCells = Grid.cellsByGridPenalty(pathFinder, true);
				ArrayList<FieldCell> saferCells = Grid.orderCellsByListsIndex(
						new ArrayList[]{hunterFarthestCells, enemyFarthestCells, gridPenaltyCells});
				dest = saferCells.get(0);
			} else {
				dest = enemyPos;
			}
		}
		return dest;
	}

	private List<BattlefieldNode> getPath() {
		if (path == null) {
			path = pathFinder.findPath(getDestiny());
		}
		return path;
	}
}
