package warrior.profile;

import warrior.strategy.PlayerEstrategy;

import java.util.ArrayList;

public class WarriorProfile {
    private final int HISTORY_SIZE = 7;
    private ArrayList<Integer> recentResults = new ArrayList();
    private int kills = 0;
    private int deaths = 0;
    private PlayerEstrategy playerStrategy;
    private String name;
    private int health;
    private int defense;
    private int strength;
    private int speed;
    private int range;

    public WarriorProfile(String name, int health, int defense, int strength, int speed, int range,
                          PlayerEstrategy pe) {
        this.name = name;
        this.health = health;
        this.defense = defense;
        this.strength = strength;
        this.speed = speed;
        this.range = range;
        this.playerStrategy = pe;
        resetStatics();
    }

    public int getHealth() {
        return health;
    }

    public int getDefense() {
        return defense;
    }

    public int getStrength() {
        return strength;
    }

    public int getSpeed() {
        return speed;
    }

    public int getRange() {
        return range;
    }

    public PlayerEstrategy getPlayerStrategy() {
        return playerStrategy;
    }

    public String getName() {
        return name;
    }

    public int getKills() {
        return kills;
    }

    public void addKills() {
        this.kills++;
        addToRecentResults(1);
        printProfileBalance();
    }

    public int getDeaths() {
        return deaths;
    }

    public void addDeaths() {
        this.deaths++;
        addToRecentResults(-1);
        printProfileBalance();
    }

    public int getBalance() {
        return kills - deaths;
    }

    public int getRecentBalance() {
        return recentResults.stream().mapToInt(Integer::intValue).sum();
    }

    public void resetStatics() {
        kills = 0;
        deaths = 0;
        recentResults.clear();
        // hack to give the profile an extra chance to loose at the very beginning
        recentResults.add(1);
    }

    private void addToRecentResults(int result) {
        recentResults.add(result);
        if (recentResults.size() > HISTORY_SIZE) {
            recentResults.remove(0);
        }
    }

    private void printProfileBalance() {
        System.out.println("    ----> Profile Balance: " + getBalance() + " - Recent Balance: " + getRecentBalance());
    }
}
