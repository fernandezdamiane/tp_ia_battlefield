package warrior.profile;

import helper.Config;
import warrior.strategy.PlayerEstrategy;
import warrior.strategy.impl.EscapeOrAttackPlayerStrategy;
import warrior.strategy.impl.FollowAttackAndHidePlayerStrategy;

import java.util.ArrayList;

public class WarriorProfileFactory {
    private static ArrayList<WarriorProfile> profilesCarousel = new ArrayList<>();

    public static void setProfilesCarousel(ArrayList<WarriorProfile> profilesCarousel) {
        WarriorProfileFactory.profilesCarousel = profilesCarousel;
    }

    public static WarriorProfile getNext() {
        WarriorProfile wp = profilesCarousel.remove(0);
        wp.resetStatics();
        profilesCarousel.add(wp);
        System.out.println("Nuevo Profile en uso: " + wp.getName());
        return wp;
    }

    public static WarriorProfile createEscapist() {
        return createByFactor(
            "Escapista Cagon",
            0.05f,
            0.05f,
            0.15f,
            0.7f,
            0.05f,
            new EscapeOrAttackPlayerStrategy(true));
    }

    public static WarriorProfile createStrongEscapist() {
        return createByFactor(
            "Escapista Aguantador",
            0.3f,
            0.3f,
            0.05f,
            0.3f,
            0.05f,
            new EscapeOrAttackPlayerStrategy(true));
    }

    public static WarriorProfile createAttacker() {
        return createByFactor(
            "Atacante Drogado",
            0.05f,
            0.05f,
            0.5f,
            0.35f,
            0.05f,
            new EscapeOrAttackPlayerStrategy(false));
    }

    public static WarriorProfile createARambo() {
        return createByFactor(
            "Rambo",
            0.3f,
            0.3f,
            0.2f,
            0.1f,
            0.1f,
            new EscapeOrAttackPlayerStrategy(false));
    }

    public static WarriorProfile createShadowAttacker() {
        return createByFactor(
            "Atacante De Las Sombras",
            0.05f,
            0.05f,
            0.25f,
            0.4f,
            0.25f,
            new FollowAttackAndHidePlayerStrategy());
    }

    public static WarriorProfile createSteadyShadowAttacker() {
        return createByFactor(
            "Atacante De Las Sombras Equilibrado",
            0.2f,
            0.2f,
            0.2f,
            0.2f,
            0.2f,
            new FollowAttackAndHidePlayerStrategy());
    }

    private static WarriorProfile createByFactor(String name, float healthFactor, float defenseFactor,
                                                 float strengthFactor,  float speedFactor, float rangeFactor,
                                                 PlayerEstrategy pe) {
        float factorSum = healthFactor + defenseFactor + strengthFactor + speedFactor + rangeFactor;
        if (healthFactor + defenseFactor + strengthFactor + speedFactor + rangeFactor != 1f) {
            throw new RuntimeException("The " + name + " Points Factor sum must be 1 but it is" + factorSum);
        }
        int maxPoints = Config.cf().getMaxPointsPerWarrior();
        int speed = Math.min(Math.max((int)Math.floor(maxPoints * speedFactor), 1), Config.maxSpeedNeeded());
        int range = Math.min(Math.max((int)Math.floor(maxPoints * rangeFactor), 1), Config.cf().getMaxRangeForWarrior());
        int health = Math.max((int)Math.floor(maxPoints * healthFactor), 1);
        int defence = Math.max((int)Math.floor(maxPoints * defenseFactor), 1);
        int strength = Math.max((int)Math.floor(maxPoints * strengthFactor), 1);
        // If are there unassigned points, lets use it for health.
        health += maxPoints - health - defence - strength - speed - range;
        return new WarriorProfile(name, health, defence, strength, speed, range, pe);

    }
}
